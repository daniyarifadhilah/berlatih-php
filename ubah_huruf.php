<?php
function ubah_huruf($string){
//kode di sini
$huruf = "abcdefghijklmnopqrstuvwxyz";
$hasil = "";
for ($x = 0; $x < strlen($string); $x++) {
    $posisi = strrpos($huruf, $string[$x]);
    $hasil .= substr($huruf, $posisi + 1, 1);
}
return $hasil;
}

// TEST CASES
echo "wow : " . ubah_huruf('wow') . "<br>"; // xpx
echo "developer : " . ubah_huruf('developer') . "<br>"; // efwfmpqfs
echo "laravel : " . ubah_huruf('laravel') . "<br>"; // mbsbwfm
echo "keren : " . ubah_huruf('keren') . "<br>"; // lfsfo
echo "semangat : " . ubah_huruf('semangat') . "<br>"; // tfnbohbu

?>
