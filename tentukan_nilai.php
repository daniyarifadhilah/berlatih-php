<?php
function tentukan_nilai($number)
{
    //  kode disini
    $hasil = "";
    if ($number >= 85 && $number <= 100 ) {
        $hasil .= "Sangat Baik";
    } else if ($number >= 70 && $number = 85 ) {
        $hasil .= "Baik";
    } else if ($number >= 60 && $number < 70 ) {
        $hasil .= "Cukup";
    } else { $hasil .= "Kurang";
    }
    return $hasil;
}

//TEST CASES
echo "<h2> Menentukan Nilai <h3><br><br>";
echo "Nilai 98 : " . tentukan_nilai(98) . "<br>"; //Sangat Baik
echo "Nilai 76 : " . tentukan_nilai(76) . "<br>"; //Baik
echo "Nilai 67 : " . tentukan_nilai(67) . "<br>"; //Cukup
echo "Nilai 43 : " . tentukan_nilai(43) . "<br>"; //Kurang
?>